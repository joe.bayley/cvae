#!/usr/bin/env python
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from torchvision import datasets, transforms
import matplotlib.pyplot as plt
import numpy as np
import os
import time
import sys
import pickle
import scipy.stats as st
from cvae import CVAE
import gen_data
import copy
import corner

def adjust_learning_rate(lr, optimizer, epoch, factor = 0.999, epoch_num = 5):
    """Sets the learning rate to the initial LR decayed by a factor 0.999 (factor) every 5 (epoch_num) epochs"""
    lr = lr * (factor ** (epoch // epoch_num))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

def KL(mu_1,log_var_1,mu_2,log_var_2):
    """Gaussian KL divergence between two distributions"""
    sigma_1 = torch.exp(0.5 * (log_var_1))
    sigma_2 = torch.exp(0.5 * (log_var_2))
    t2 = torch.log(sigma_2/sigma_1)
    t3 = (torch.square(mu_2 - mu_1) + torch.square(sigma_1))/(2*torch.square(sigma_2))
    # take sum of KL divergences in the latent space  
    kl_loss = torch.mean(t2 + t3 - 0.5,dim=1)
    return kl_loss


def log_lik(par, mu, log_var):
    """Gaussian log-likelihood """ 
    sigma_square = torch.exp(log_var)
    arr = torch.log(2*np.pi*sigma_square) + torch.square(par-mu)/sigma_square
    # takes the mean of the likelihoods in the parameter space
    return -0.5*torch.mean(arr,dim=1)

def train_batch(epoch, model, optimizer, device, batch, labels, ramp=1.0, train = True):
    """ Train one batch"""
    model.train(train)
    if train:
        optimizer.zero_grad()
    length = float(batch.size(0))
    # calculate r2, q and r1 means and variances
    mu_x, log_var_x, mu_q, log_var_q, mu_r, log_var_r = model(batch,labels)

    # get the KL loss funtion
    kl_loss_b = KL(mu_q,log_var_q,mu_r,log_var_r)

    # get the L loss (evaluate gaussian at true parameter)
    L_loss_b = log_lik(labels, mu_x, log_var_x)

    # get the mean losses over the batch
    L_loss = torch.mean(L_loss_b)
    kl_loss = torch.mean(kl_loss_b)

    # calcualte total loss
    loss = -L_loss + ramp*kl_loss
    if train:
        loss.backward()
        # update the weights                                                                                                                              
        optimizer.step()

    return loss.item(), kl_loss.item(), L_loss.item()

def train(model, device, epochs, train_iterator, learning_rate, validation_iterator, ramp_start = -1,ramp_end =-1,save_dir = "./"):
    """ Train a CVAE"""

    # define the opimiser
    optimizer = torch.optim.Adam(model.parameters(),lr=learning_rate)

    # initialise variables
    train_losses = []
    kl_losses = []
    lik_losses = []
    val_losses = []
    val_kl_losses = []
    val_lik_losses = []
    kl_start = ramp_start
    kl_end = ramp_end

    # loop for each training epoch
    for epoch in range(epochs):

        # adjust the learning rate after each epoch
        if epoch > 2*kl_start:
            adjust_learning_rate(learning_rate, optimizer, epoch, factor = 0.999)

        # apply a ramp to the learning rate at a specific epoch
        ramp = 0.0
        if epoch>kl_start and epoch<=kl_end:
            ramp = (np.log(epoch)-np.log(kl_start))/(np.log(kl_end)-np.log(kl_start)) 
        elif epoch>kl_end:
            ramp = 1.0 
        

        # Train a batch
        for local_batch, local_labels in train_iterator:
            # Transfer to GPU            
            local_batch, local_labels = local_batch.to(device), local_labels.to(device)
            # train a batch
            train_loss,kl_loss,lik_loss = train_batch(epoch, model, optimizer, device, local_batch,local_labels, ramp=ramp, train=True)
        train_losses.append(train_loss)
        kl_losses.append(kl_loss)
        lik_losses.append(lik_loss)

        # run on validation data
        for val_batch, val_labels in validation_iterator:
            # Transfer to GPU            
            val_batch, val_labels = val_batch.to(device), val_labels.to(device)
            val_loss,val_kl_loss,val_lik_loss = train_batch(epoch, model, optimizer, device, val_batch, val_labels, ramp=ramp, train=False)
        val_losses.append(val_loss)
        val_kl_losses.append(val_kl_loss)
        val_lik_losses.append(val_lik_loss)

        if epoch % int(epochs/10) == 0:                                                                                                              
            print("Train:      Epoch: {}, Training loss: {}, kl_loss: {}, l_loss:{}".format(epoch,train_loss,kl_loss,lik_loss))
            print("Validation: Epoch: {}, Training loss: {}, kl_loss: {}, l_loss:{}".format(epoch,val_loss,val_kl_loss,val_lik_loss))
            loss_plot(save_dir, train_losses, kl_losses, lik_losses, val_losses, val_kl_losses, val_lik_losses)

    return train_losses, kl_losses, lik_losses, val_losses, val_kl_losses, val_lik_losses

def run(model,test_it,num_samples = 500,device="cpu"):
    """ run the model to extract samples, put all test data into one batch"""
    # set the evaluation mode
    model.eval()
    
    # test loss for the data
    test_loss = 0
    samples = {}
    # we don't need to track the gradients, since we are not updating the parameters during evaluation / testing
    with torch.no_grad():
        for local_batch, local_labels in test_it:
            # Transfer to GPU                                                                                                                         
            local_batch, local_labels = local_batch.to(device), local_labels.to(device)
            # test the model
            samples = model.test(local_batch,num_samples)
            truths = local_labels
            break
    return samples,truths.cpu().numpy()


def test_model(save_dir, num_test, model, length=50, noise_std = 0.01):


    run_data, val_data = load_train_data(length = length,  noise_std=noise_std, train_num=num_test, val_num=0)

    run_iterator = DataLoader(run_data, batch_size=num_test)

    samples, truths = run(model, run_iterator,5000,device=device)


    post_savedir = os.path.join(save_dir,"test_posteriors")
    for d in [post_savedir]:
        if not os.path.isdir(d):
            os.makedirs(d)

    with open(os.path.join(save_dir,"samples.pkl"),"wb") as f:
        pickle.dump(samples,f)

    labels = ["m", "c"]

    for ind in range(num_test):
        figad = corner.corner(samples[ind],truths=truths[ind],labels=labels,color="C0",truth_color='C3',show_titles=True,hist_kwargs={"density":True})
        figad.set_size_inches(11,11)
        figad.savefig(os.path.join(post_savedir,"injection_{}".format(ind)))
        plt.close(figad)



def loss_plot(save_dir, loss, kl_loss, l_loss, val_loss, val_kl_loss, val_l_loss, space = 10):
    """ Plot the loss curves"""
    fig, ax = plt.subplots(nrows = 2,figsize=(15,15))
    xs = np.linspace(0,len(loss), len(loss))
    # plot the kl loss, L loss and total loss (all log losses)
    ax[0].plot(xs,loss,label="loss",color="C0",alpha=0.5)
    ax[1].plot(xs, np.array(kl_loss),label="KL loss",color="C1",alpha=0.5)
    ax[0].plot(xs,-np.array(l_loss),label="L loss",color="C2",alpha=0.5)

    # validation losses
    ax[0].plot(xs,val_loss,label="val loss", color="C0", ls="--", lw=3, alpha=0.5)
    ax[1].plot(xs,np.array(val_kl_loss), label="val KL loss", color="C1", ls="--", lw=3, alpha=0.5)
    ax[0].plot(xs,-np.array(val_l_loss), label="val L loss", color="C2", ls="--", lw=3, alpha=0.5)

    ax[1].set_xlabel("Epoch")
    ax[0].legend(fontsize=20)
    ax[1].legend(fontsize=20)
    ax[0].grid()
    ax[1].grid()
    ax[1].set_yscale("log")
    ax[0].set_yscale("symlog")
    fig.savefig(os.path.join(save_dir, "loss.png"))
    plt.close(fig)


def load_train_data(length = 50, noise_std = 0.1, train_num=1000, val_num = 100):
    """Load in training data for a simple line with Gaussian noise with std noise_std"""
    xs = np.linspace(0,1.0,length)
    train_data = gen_data.GenLine(xs = xs)
    train_data.gen_train_data(train_num, noise_std = noise_std)
    validation_data = gen_data.GenLine(xs = xs)
    validation_data.gen_train_data(val_num, noise_std = noise_std)
    
    return train_data, validation_data
    

def run_cvae(num_parameters, num_latent, num_fc_neurons, num_fc_layers, num_conv_layers, num_conv_filters, conv_size, learning_rate, device, train_epochs,  stride, maxpool_size, length = 469,batch = 1000,num=1e4, dropout=0.0, noise_std=0.01, save_root = "./", ramp_start=-1,ramp_end=-1, save_dir = None):
    """
    run cvae on given data model
    """
    # define save directory
    if save_dir is None:
        save_dir = os.path.join(save_root,"opt_dat{}_num{}_noise{}_normed/ep{}_lr{}_cnum{}_csize{}_cfiltnum{}_mpsize{}_fcnum{}_fcsize{}_drop{}_latent{}_batch{}_ramp{}_{}".format(num, length, noise_std,train_epochs, learning_rate, num_conv_layers, conv_size, num_conv_filters, maxpool_size, num_fc_layers, num_fc_neurons, dropout, num_latent, batch, ramp_start,ramp_end))    

    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)

    # load the training data
    train_data, validation_data = load_train_data(length=length ,noise_std=noise_std)
    train_iterator = DataLoader(train_data, batch_size=batch, shuffle=True)
    validation_iterator = DataLoader(validation_data, batch_size=batch, shuffle=True)
    
    # define the layers of a network as a list
    fc_layers = [num_fc_neurons for i in range(num_fc_layers)]
    conv_layers = [(num_conv_filters, conv_size, maxpool_size) for i in range(num_conv_layers)]
    
    # define the model
    model = CVAE(input_dim=length,par_dim=num_parameters,latent_dim=num_latent,fc_layers = fc_layers, conv_layers=conv_layers, stride=stride,device=device,dropout=dropout).to(device)

    # train the model
    losses, kl_loss, l_loss, val_loss, val_kl_loss, val_l_loss= train(model, device, train_epochs, train_iterator, learning_rate, validation_iterator, ramp_start=ramp_start,ramp_end=ramp_end,save_dir=save_dir)

    # save model and plot loss
    loss_plot(save_dir, losses, kl_loss, l_loss, val_loss, val_kl_loss, val_l_loss)
    torch.save(model, os.path.join(save_dir,"model.pt"))
    test_model(save_dir, 10, model, length=length, noise_std = noise_std)


if __name__ == "__main__":


    os.environ["CUDA_VISIBLE_DEVICES"] = "0,1,2,3"

    # set your root dir here
    root_dir = "./"

    # make double the default type for tensors
    torch.set_default_tensor_type(torch.DoubleTensor)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    try:
        print(device, torch.cuda.get_device_name(device))
    except:
        print(device)

    num = int(1e4)
    lengths = [50]
    noise_std =[0.01]

    # network parameters
    train_epochs = [500]
    ramp_start = 100
    ramp_end = 300

    num_parameters = 2
    
    for ln in lengths:
        for nv in noise_std:
            for t_ep in train_epochs:
                run_cvae(num_parameters=num_parameters,
                         num_latent=6,
                         num_fc_neurons=8,
                         num_fc_layers=0,
                         learning_rate=5e-4,
                         device=device,
                         train_epochs=t_ep,
                         num_conv_layers=1,
                         num_conv_filters = 8,
                         conv_size=4,
                         stride=1,
                         maxpool_size=2,
                         length = ln,
                         batch = int(200),
                         num=num,
                         dropout=0,
                         ramp_start=ramp_start,
                         ramp_end=ramp_end,
                         noise_std = nv,
                         save_root = root_dir)
                
