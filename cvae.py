import torch
import torch.nn as nn
import numpy as np

class CVAE(nn.Module):

    def __init__(self, input_dim, par_dim, latent_dim, fc_layers = [], conv_layers = [], stride = 0, device="cpu", dropout=0.1):
        """
        args
        ----------
        input_dim: int
            length of input time series
        par_dim: int
            size of parameters at output of final encoder
        latent_dim: int
            number of variables in latent space
        fc_layers: list
            size of each convolutional layer, i.e. for 3 layers [32,16,16]
        conv_layers: list
            convolutional layers in format [(num_filters, conv_size, maxpool_size), (...), ]
        par_dim: int
            number of parameter to estimate
        """
        super().__init__()
        # define useful variables of network
        self.device = device
        self.input_dim = input_dim
        self.par_dim = par_dim
        self.latent_dim = latent_dim

        # convolutional parts
        self.fc_layers = fc_layers
        self.conv_layers = conv_layers
        self.num_conv = len(self.conv_layers)
        self.stride = stride

        # define network parameters
        self.activation = nn.ReLU()
        self.drop = nn.Dropout(p=dropout)

        self.small_const = 1e-6

        # encoder r1(z|y) 
        self.rencoder_conv, self.rencoder_lin = self.create_network("r", self.input_dim, self.latent_dim, append_dim=0, fc_layers = self.fc_layers, conv_layers = self.conv_layers)

        # encoder q(z|x, y) 
        self.qencoder_conv, self.qencoder_lin = self.create_network("q", self.input_dim, self.latent_dim, append_dim=self.par_dim, fc_layers = self.fc_layers, conv_layers = self.conv_layers)

        # encoder r2(x|z, y) 
        self.decoder_conv, self.decoder_lin = self.create_network("d", self.input_dim, self.par_dim, append_dim=self.latent_dim, fc_layers = self.fc_layers, conv_layers = self.conv_layers)
        
    def create_network(self, name, input_dim, output_dim, append_dim=0, mean=True, variance=True, fc_layers=[], conv_layers=[]):
        """ Create the network"""
        # set up sequatial layers
        conv_network = nn.Sequential()
        lin_network = nn.Sequential()
        # initialise variables
        layer_out_sizes = []
        num_conv = len(conv_layers)
        num_fc = len(fc_layers)
        inchannels = 1
        insize = self.input_dim
        
        for i in range(num_conv):
            padding = int(conv_layers[i][1]/2.) # define paddin
            maxpool = nn.MaxPool1d(conv_layers[i][2])
            conv_network.add_module("r_conv{}".format(i),module = nn.Conv1d(inchannels, conv_layers[i][0], conv_layers[i][1], stride = self.stride,padding=padding))     # add convolutional filter
            conv_network.add_module("act_r_conv{}".format(i), module = self.activation) # add activation on convolution layer
            conv_network.add_module("pool_r_conv{}".format(i),module = maxpool) # add maxpooling layer
            outsize = int(self.conv_out_size(insize, padding, 1, conv_layers[i][1], self.stride)/conv_layers[i][2]) # calculate the output layer size
            layer_out_sizes.append((conv_layers[i][0],outsize)) # add output layer to list
            insize = outsize
            inchannels = conv_layers[i][0]

        # define input to fully connected layer
        lin_input_size = np.prod(layer_out_sizes[-1]) if num_conv > 0 else self.input_dim 
        # define if parameters are appended to data at input
        if append_dim:
            lin_input_size += append_dim
        layer_size = int(lin_input_size)
        # add fully connected layers
        for i in range(num_fc):
            lin_network.add_module("r_lin{}".format(i),module=nn.Linear(layer_size, fc_layers[i]))
            lin_network.add_module("act_r_lin{}".format(i),module=self.activation)
            layer_size = fc_layers[i]
        # output mean and variance of gaussian with size of latent space
        if mean:
            setattr(self,"mu_{}".format(name[0]),nn.Linear(layer_size, output_dim))
        if variance:
            setattr(self,"log_var_{}".format(name[0]),nn.Linear(layer_size, output_dim))
        
        return conv_network, lin_network

    def conv_out_size(self, in_dim, padding, dilation, kernel, stride):
        """ Function which defined output size of maxpooling layer"""
        return int((in_dim + 2*padding - dilation*(kernel-1)-1)/stride + 1)
        
    def encode_r(self,y):
        """ encoder r1(z|y) , takes in observation y"""
        # set up encoder layer
        conv = self.rencoder_conv(torch.reshape(y, (-1, 1, self.input_dim))) if self.num_conv > 0 else y
        # flatten output of conv layers
        lin_in = torch.flatten(conv,start_dim=1)
        # set up linear layer
        lin = self.rencoder_lin(lin_in)
        z_mu = self.mu_r(lin) # latent means
        z_log_var = self.log_var_r(lin) + self.small_const # latent variances
        return z_mu, z_log_var
    
    def encode_q(self,y,par):
        """ encoder q(z|x, y) , takes in observation y and paramters par (x)"""
        conv = self.qencoder_conv(torch.reshape(y, (-1, 1, self.input_dim))) if self.num_conv > 0 else y
        lin_in = torch.cat([torch.flatten(conv,start_dim=1), par],1)
        lin = self.qencoder_lin(lin_in)
        z_mu = self.mu_q(lin)  # latent means
        z_log_var = self.log_var_q(lin) + self.small_const # latent vairances
        return z_mu, z_log_var
    
    def decode(self, z, y):
        """ decoder r2(x|z, y) , takes in observation y and latent paramters z"""
        conv = self.decoder_conv(torch.reshape(y, (-1, 1, self.input_dim))) if self.num_conv > 0 else y
        lin_in = torch.cat([torch.flatten(conv,start_dim=1),z],1) 
        lin = self.decoder_lin(lin_in)
        par_mu = self.mu_d(lin) # parameter means
        par_log_var = self.log_var_d(lin) + self.small_const # parameter variances
        return par_mu, par_log_var

    def sample(self, mean, log_var, num_batch, dim):
        """ Sample trom a gaussian with given mean and log variance 
        (takes in a number (dim) of means and variances, and samples num_batch times)"""
        std = torch.exp(0.5 * (log_var))
        eps = torch.randn([num_batch, dim]).to(self.device)
        sample = torch.add(torch.mul(eps,std),mean)
        return sample

    def forward(self, y, par):
        """forward pass for training"""
        batch_size = y.size(0) # set the batch size
        # encode data into latent space
        mu_r, log_var_r = self.encode_r(y) # encode r1(z|y)
        mu_q, log_var_q = self.encode_q(y, par) # encode q(z|x, y)
        
        # sample z from gaussian with mean and variance from q(z|x, y)
        z_sample = self.sample(mu_q, log_var_q, batch_size, self.latent_dim)
        # get the mean and variance in parameter space from decoder
        mu_par, log_var_par = self.decode(z_sample,y) # decode r2(x|z, y)                                                                                        
        
        return mu_par, log_var_par, mu_q, log_var_q, mu_r, log_var_r
    
    def test(model, y, num_samples):
        """generating samples when testing the network """
        num_data = y.size(0)                                                                                                                                                     
        x_samples = []
        # encode the data into latent space with r1(z,y)          
        mu_r, log_var_r = model.encode_r(y) # encode r1(z|y) 
        # get the latent space samples 
        for i in range(num_data):
            z_sample = model.sample(mu_r[i], log_var_r[i], num_samples, model.latent_dim)
            # input the latent space samples into decoder r2(x|z, y)  
            ys = y[i].repeat(1,num_samples).view(-1, y.size(1)) # repeat data so same size as the z samples
            mu_par, log_var_par = model.decode(z_sample,ys) # decode r2(x|z, y) from z        
            # sample parameter space from returned mean and variance 
            samp = model.sample(mu_par, log_var_par, num_samples, model.par_dim)
            # add samples to list    
            x_samples.append(samp.cpu().numpy())
        
        return np.array(x_samples)

