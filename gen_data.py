import pickle as pkl
import numpy as np
import time
from datetime import datetime 
import os
import matplotlib.pyplot as plt
from astropy.coordinates import SkyCoord
from astropy import units as u
import random


class GenLine:
    def __init__(self, xs=np.linspace(0,1.4,469)):
        """
        Generate a simple stright line y = mx + c, where data is scaled between 0 and 1
        """
        self.xs = xs

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, index):
        return [self.data[index: index+1][0][0],self.data[index: index+1][0][1]]

    def gen_line(self, noise_std = 0.01):
        self.maxslope = 1
        self.maxoff = 1
        slope = np.random.rand(1)[0]
        off = np.random.rand(1)[0]
        ys = self.line_model(self.xs, slope, off) + np.random.normal(loc = 0, scale = noise_std, size = len(self.xs))
        return ys/(max(self.xs)*self.maxslope + self.maxoff), np.array([slope, off])

    def line_model(self, x, m, c):
        return (m*x + c)

    def transform_samples(self, samples):
        return samples

    def gen_train_data(self, numdata, noise_std = 0.01):
        self.start_load = time.time()
        dataset = np.array([self.gen_line(noise_std = noise_std) for i in range(numdata)])
        self.num_data = numdata
        self.data = dataset
        self.end_load = time.time()

    def gen_test_data(self, numdata, noise_std = 0.01):
        dataset = np.array([self.gen_line(noise_std=noise_std) for i in range(numdata)])
        self.data = dataset

    def log_likelihood(self, params,x,y,model, noise_std):
        m, c, sig = params
        sig = noise_std
        if m < 0.0 or m > self.maxslope:
            return -np.inf
        if c < 0.0 or c > self.maxoff:
            return -np.inf
        else:
            N = len(x)
            mu = model(x,m,c)
            test = -np.log(sig*np.sqrt(2*np.pi))
            lik_func = np.sum(test - ((y - mu)**2/(2*sig*sig)))
            return lik_func


    def run_emcee(self,y,inj=None, noise_std = 0.01):
        
        ndim, nwalkers = 3, 50
        #ranges = np.array([[inj[0] - 0.2,inj[1]-0.2, 0],[inj[0]+0.2,inj[0]+0.2, 2]])           
        ranges = np.array([[ 0, 0, 0],[1, 1, 2]])
        pos = [ranges[0] + np.random.rand(ndim)*(ranges[1] - ranges[0]) for i in range(nwalkers)]

        model = lambda x, m, c: self.line_model(x,m,c)/(max(self.xs)*self.maxslope + self.maxoff)

        samples = []
        for dat in self.num_data:
            sampler = emcee.EnsembleSampler(nwalkers, ndim, log_likelihood, args=(self.xs,y,model, noise_std))
        
            out = sampler.run_mcmc(pos, 2000)
        
            samples.append(sampler.chain[:, 1000:, :].reshape((-1, ndim)))
        
        return samples

    
